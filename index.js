/*
This file is part of Plangen API.

Plangen API is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Plangen API is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with Plangen API.  If not, see <http://www.gnu.org/licenses/>.
*/

const express = require('express');
const cors = require('cors');
const app = express();
const port = 3001;

const LICENSE = `
---------------------------------------------------------------------------
Plangen API is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Plangen API is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with Plangen API.  If not, see <http://www.gnu.org/licenses/>.
---------------------------------------------------------------------------
`

const Plangen = require ('./modules/solsys.js')

app.use(cors());         // make this stuff callable
app.use(express.json()); // use json data

app.get("/", (req, res) => {
    res.send("Hello world from express!");
});

app.get("/solsys", (req,res) => {
    const solsys = Plangen.makeSampleSolsys();
    res.json(solsys);

})

app.post("/solsys", (req,res) => {
    p = req.body;
    const solsys = Plangen.makeCustomSolsys(p.starclass, p.starlum);
    res.json(solsys);

})

app.listen(port, () => console.log(`
${LICENSE}

solsysapi running on port ${port}!`));