/*
This file is part of Plangen API.

Plangen API is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Plangen API is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with Plangen API.  If not, see <http://www.gnu.org/licenses/>.
*/

var roller = require ('rpg-dice-roller');
var namer = require('random-name')
const planet = require('./planet.js');

var DiceRoll = roller.DiceRoll;
var Planet = planet.Planet;
var PlanetEnum = planet.PlanetEnum;

const StarsEnum = {
    O : "O",
    B : "B",
    A : "A",
    F : "F",
    G : "G",
    K : "K",
    M : "M"
}

const DIST_1_3 = ["B","C"]
const DIST_4_5 = ["A","B","C"]
const DIST_6_7 = ["A","B","B","C"]
const DIST_8 =   ["A","A","B","B","C"]

const RANDOM_MAIN_SEQ_STARS=["F","G","K","M"];

const DISTROS = [DIST_1_3, DIST_1_3, DIST_1_3, DIST_4_5, DIST_4_5, DIST_6_7, DIST_6_7];

const ZONE_DIST= {
    "A" : [
        { min : 0, max : 5, proc : makeAsteroids },
        { min : 6, max : 60, proc : makeRock },
        { min : 61, max :70, proc : makeDesert },
        { min : 71, max : 100, proc : makeHostile },
    ],
    "B" : [
        { min : 0, max : 5, proc : makeAsteroids },
        { min : 6, max : 8, proc : makeGiant },
        { min : 9, max : 40, proc : makeRock },
        { min : 41, max :60, proc : makeDesert },
        { min : 61, max :70, proc : makeHostile },
        { min : 71, max :85, proc : makeMarginal },
        { min : 86, max : 100, proc : makeEarthlike },
    ],
    "C" : [
        { min : 0, max : 5, proc : makeAsteroids },
        { min : 6, max : 75, proc : makeGiant },
        { min : 76, max :80, proc : makeRock },
        { min : 81, max :90, proc : makeCold },
        { min : 91, max :95, proc : makeHostile },
        { min : 96, max : 100, proc : makeHostile },
    ],
    
}

function makeAsteroids(planetName=namer.place()+" Asteroid Belt") {
    p = new Planet(planetName, PlanetEnum.Asteroids, new DiceRoll("3d6").total * 100, new DiceRoll("2d10").total * 1000 )
    return p;    
}

function makeGiant(planetName=namer.place()) {
    p = new Planet(planetName, PlanetEnum.Giant, new DiceRoll("3d6").total * 10000, new DiceRoll("2d10").total )
    return p;    
}

function makeRock(planetName=namer.place()) {
    let moons  = (new DiceRoll("1d6").total * new DiceRoll("1d6").total) / 10
    p = new Planet(planetName, PlanetEnum.Rock, new DiceRoll("1d10").total * 1000, Math.floor(moons) )
    return p;    
}

function makeCold(planetName=namer.place()) {
    let moons  = (new DiceRoll("1d6").total * new DiceRoll("1d6").total) / 10
    p = new Planet(planetName, PlanetEnum.Cold, new DiceRoll("1d10").total * 1000, Math.floor(moons) )
    return p;    
}


function makeDesert(planetName=namer.place()) {
    let moons  = (new DiceRoll("1d6").total * new DiceRoll("1d6").total) / 10
    p = new Planet(planetName, PlanetEnum.Desert, new DiceRoll("2d6+2").total * 1000, Math.floor(moons) )
    return p;    
}


function makeHostile(planetName=namer.place()) {
    let moons  = (new DiceRoll("1d6").total * new DiceRoll("1d6").total) / 10
    p = new Planet(planetName, PlanetEnum.Hostile, new DiceRoll("3d6+1").total * 1000, Math.floor(moons) )
    return p;    
}


function makeMarginal(planetName=namer.place()) {
    let moons  = (new DiceRoll("1d6").total * new DiceRoll("1d6").total) / 10
    p = new Planet(planetName, PlanetEnum.Marginal, new DiceRoll("2d6+5").total * 1000, Math.floor(moons) )
    return p;    
}


function makeEarthlike(planetName=namer.place()) {
    let moons  = (new DiceRoll("1d6").total * new DiceRoll("1d6").total) / 10
    p = new Planet(planetName, PlanetEnum.Earthlike, new DiceRoll("2d6+5").total * 1000, Math.floor(moons) )
    return p;    
}

class SolarSystem {
    constructor(starName=namer.place(), starType=StarsEnum.O, starLum=2) {
        this.starName = starName;
        this.starType = starType;
        this.starLum = starLum;
        this.planets=[];
    }

    ping() {
        console.log("Pong");
    }

    addPlanet(p) {
        if (p.planetType == PlanetEnum.Earthlike || p.planetType == PlanetEnum.Marginal) {
            this.addFeaturesToPlanet(p);
        }
        this.planets.push(p);
    }

    calcYear(p) {
        let orbit = 365;
        switch (this.starType) {            
            case StarsEnum.F:
                orbit = this.starLum < 5 ? 600 + 3 * (new DiceRoll("2d100")).total : 400 + 2 * (new DiceRoll("2d100")).total
                break;
            case StarsEnum.G:
                orbit = this.starLum < 5 ? 270 +  (new DiceRoll("2d100")).total : 150 + (new DiceRoll("2d100")).total
                break;
            case StarsEnum.K:
                orbit = this.starLum < 5 ? 150 +  (new DiceRoll("1d100")).total : 70 + (new DiceRoll("1d100")).total
                break;
            case StarsEnum.M:
                orbit = this.starLum < 5 ? 50 + 0.5 * (new DiceRoll("1d100")).total : 25 + 0.4 * (new DiceRoll("1d100")).total
                break;
        }
        return orbit;
    }

    calcTemp(p) {
        let temperature = (p.planetType == PlanetEnum.Earthlike) ? 
            (new DiceRoll("2d6")).total - 4 : (new DiceRoll("1d10+1d6")).total - 6
        return temperature *= 5;
    }

    addFeaturesToPlanet() {
        let years = []
        let temps = []
        this.planets
            .filter(pl => pl.planetType == PlanetEnum.Earthlike ||  pl.planetType == PlanetEnum.Marginal) 
            .forEach( p => {
                years.push(this.calcYear(p));
                temps.push(this.calcTemp(p));
                p.calcDay();
                p.calcDensity()
                p.calcExtraFeatures();
                p.calcBiologicalDetails();
            });

        years.sort()
        temps.sort( (a,b) => b-a);
        let cnt = 0;
        this.planets
            .filter(pl => pl.planetType == PlanetEnum.Earthlike 
                       || pl.planetType == PlanetEnum.Marginal) 
            .forEach( p => {
                p.year = years[cnt];
                p.temperature = temps[cnt];
                p.calcWater()
                cnt++;
            });
    }
}


get_num_planets = (roll,prob, diceString) => {
    let dr = new DiceRoll(diceString);
    return (roll <= prob) ? dr.total : 0;
}

num_planets = (starType) => {
    let prob = new DiceRoll("1d100").total;
    let num_planets = 0;
    switch(starType) {
        case StarsEnum.O:
        case StarsEnum.B:
            return get_num_planets(prob,10,"1d10"); break;
        case StarsEnum.A:
            return get_num_planets(prob,50,"1d10"); break;
        case StarsEnum.F:
        case StarsEnum.G:
            return get_num_planets(prob,99,"2d6+3"); break;
        case StarsEnum.K:
            return get_num_planets(prob,99,"2d6"); break;
        case StarsEnum.M:
            return get_num_planets(prob,50,"1d6"); break;
        default:
            break;
    }
    return num_planets;
}

planet_by_zone = (zone) => {
    const dist = ZONE_DIST[zone];
    const roll = new DiceRoll("1d100").total;
    var planet = null;
    dist.forEach(e => {
        if (e.min <= roll && roll <= e.max) {
            planet =  e.proc();
            planet.zone = zone;
        }
    }); 
    return planet;
}

function makeSampleSolsys() {
    let scIdx = new DiceRoll("1d4-1").total
    let scLum = new DiceRoll("1d10-1").total
    let solClass = RANDOM_MAIN_SEQ_STARS[scIdx];
    solsys = new SolarSystem(namer.place(),solClass, scLum);
    const np = num_planets(solsys.starType);
    if (np > 0) {
        distro = np >= 7 ? DIST_8  : DISTROS[np];
        for (var h = 0; h < np; h++) {
            if ( h >= distro.length) {
                solsys.addPlanet(planet_by_zone("C"))
            }
            else {
                solsys.addPlanet(planet_by_zone(distro[h]))
            }
        }
    }
    solsys.addFeaturesToPlanet();
    return solsys;
}

function makeCustomSolsys(solClass, scLum) {
    solsys = new SolarSystem(namer.place(),solClass, scLum);
    const np = num_planets(solsys.starType);
    if (np > 0) {
        distro = np >= 7 ? DIST_8  : DISTROS[np];
        for (var h = 0; h < np; h++) {
            if ( h >= distro.length) {
                solsys.addPlanet(planet_by_zone("C"))
            }
            else {
                solsys.addPlanet(planet_by_zone(distro[h]))
            }
        }
    }
    solsys.addFeaturesToPlanet();
    return solsys;
}


exports.makeSampleSolsys = makeSampleSolsys;
exports.makeCustomSolsys = makeCustomSolsys;
exports.SolarSystem = SolarSystem;

