/*
This file is part of Plangen API.

Plangen API is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Plangen API is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with Plangen API.  If not, see <http://www.gnu.org/licenses/>.
*/

var roller = require ('rpg-dice-roller');
var namer = require('random-name');

var DiceRoll = roller.DiceRoll;

const WATER_PERC=[
    { min: -30, max: -10, water: "1d10-5", ice: "1d10+10"},
    { min: -10, max:  10, water: "1d20-5", ice: "1d10"},
    { min:  10, max:  25, water: "5d6-8", ice: "1d6-2"},
    { min:  25, max:  35, water: "5d6-8", ice: "1d6-3"},
    { min:  35, max:  50, water: "5d6-8", ice: "1d6-4"},
]

const LIFE_DEVELOPMENT_LEVEL={
    "Earthlike" : [
        { min :  1, max :  10, level:"I: Single-celled organisms only"},
        { min : 11, max :  20, level:"II: Simple invertebrates and plants (in seas)"},
        { min : 21, max :  30, level:"III: Advanced invertebrates and plants (on land)"},
        { min : 31, max :  40, level:"IV: Simple vertebrates (fish, amphibians)"},
        { min : 41, max : 100, level:"V: Advanced vertebrates (reptiles, birds, mammals)"},
    ],
    "Marginal" : [
        { min :  0, max :   1, level:"No life"},
        { min :  2, max :  30, level:"I: Single-celled organisms only"},
        { min : 31, max :  45, level:"II: Simple invertebrates and plants (in seas)"},
        { min : 46, max :  60, level:"III: Advanced invertebrates and plants (on land)"},
        { min : 61, max :  75, level:"IV: Simple vertebrates (fish, amphibians)"},
        { min : 76, max : 100, level:"V: Advanced vertebrates (reptiles, birds, mammals)"},
    ],
}

const LIFE_BIO_CHEMISTRY={
    "Earthlike" : [
        { min : 01, max:  20, label:"Earthlike"},
        { min : 21, max:  80, label:"Protein-based (frequently inedible and poisonous)"},
        { min : 81, max: 100, label:"Carbon-based (always inedible and poisonous)"},
    ],
    "Marginal" : [
        { min : 01, max:  05, label:"Earthlike"},
        { min : 06, max:  60, label:"Protein-based (frequently inedible and poisonous)"},
        { min : 61, max:  99, label:"Carbon-based (always inedible and poisonous)"},
        { min :100, max: 101, label:"Non-carbon based"},

    ]
}

const PlanetEnum = {
    Asteroids : "Asteroids",
    Giant : "Giant",
    Rock : "Rock",
    Cold : "Cold",
    Desert : "Desert",
    Hostile : "Hostile",
    Marginal : "Marginal",
    Earthlike : "Earthlike"
}

exports.PlanetEnum = PlanetEnum;

class Planet  {
    constructor (planetName = namer.place(), 
                planetType = PlanetEnum.Asteroids, 
                diameter=0, 
                noMoons = 0) {
        this.planetName = planetName;
        this.planetType = planetType;
        this.diameter = diameter;
        this.num_moons = noMoons;
        this.zone=""
        this.features=[]
    }

    calcBiologicalDetails() {
        var roll = new DiceRoll("1d100").total;
        var roll1 = new DiceRoll("1d100").total;
        var lifeDev = LIFE_DEVELOPMENT_LEVEL[this.planetType]
        var lifeChem = LIFE_BIO_CHEMISTRY[this.planetType]
        this.life_development = lifeDev.filter(f => f.min <= roll && roll <= f.max)[0].level;
        this.life_chemistry = lifeChem.filter(f => f.min <= roll1 && roll1 <= f.max)[0].label;
    }

    calcWater() {
        var wp_item = WATER_PERC.filter(it => it.min <= this.temperature && this.temperature <= it.max)[0]
        var water = 75;
        var ice = 10;
        do {
            water = new DiceRoll(wp_item.water).total*5;
            ice = new DiceRoll(wp_item.ice).total*5;
            if (water < 0) water = 0;
            if (ice < 0) ice = 0;
        } while (water + ice > 100)
        this.water = water
        this.ice = ice
        this.landmass = (100 - (water+ice))
    }

    calcDensity() {
        this.density= (new DiceRoll("1d20").total)/6.25+3
    }

    calcExtraFeatures() {
        this.gravity = Math.round((this.diameter * this.density / 700)) / 100;
        this.mineral = (this.diameter / 500)+(this.density*10)+ (new DiceRoll("1d100")).total/2 -45
    }

    calcDay() {
        let roll = this.num_moons * 10 + new DiceRoll("1d100").total;
        if (roll < 65)
            this.day =  9 + new DiceRoll("1d20").total
        else if (roll < 90)
            this.day =  20 + new DiceRoll("1d20").total
        else if (roll < 98)
            this.day =   (new DiceRoll("1d6").total * 24) + new DiceRoll("1d6").total
        else if (roll < 103)
            this.day =   (new DiceRoll("1d8").total * 24) + new DiceRoll("1d6").total
        else 
            this.day =   (new DiceRoll("1d10").total * 24) + new DiceRoll("1d6").total
    }    
}

exports.Planet = Planet;